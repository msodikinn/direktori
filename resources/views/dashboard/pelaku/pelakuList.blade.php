@extends('dashboard.base')

@section('content')

        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i>{{ __('Pelaku') }}</div>
                    <div class="card-body">
                        <div class="row"> 
                          <a href="{{ route('pelaku.create') }}" class="btn btn-primary m-2">{{ __('Add Pelaku Usaha') }}</a>
                        </div>
                        <br>
                        <table class="table table-responsive-sm table-striped">
                        <thead>
                          <tr>
                            <td>Name</td>
                            <td></td>
                            <td></td>
                            <td></td>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($notes as $note)
                            <tr>
                              <td><strong>{{ $note->name }}</strong></td>
                              <td>
                                <a href="{{ url('/pelaku/' . $note->id) }}" class="btn btn-sm btn-primary"><i class="cil-caret-right"></i></a>
                              </td>
                              <td>
                                <a href="{{ url('/pelaku/' . $note->id . '/edit') }}" class="btn btn-sm btn-warning">
                                  <i class="cil-pencil"></i></a>
                              </td>
                              <td>
                                <form action="{{ route('pelaku.destroy', $note->id ) }}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-sm btn-danger"><i class="cil-trash"></i></button>
                                </form>
                              </td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                      {{ $notes->links() }}
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection


@section('javascript')

@endsection

