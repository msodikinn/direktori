@extends('dashboard.base')

@section('content')

        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i>{{ __('Direktori Usaha') }}</div>
                    <div class="card-body">
                        <div class="row"> 
                          <a href="{{ route('direktori.create') }}" class="btn btn-primary m-2">{{ __('Add Direktori') }}</a>
                        </div>
                        <br>
                        <table class="table table-responsive-sm table-striped">
                        <thead>
                          <tr>
                            <th>Nama Pelaku Usaha</th>
                            <th>Jenis Usaha</th>
                            <th>Komoditas Utama</th>
                            <th>Alamat</th>
                            <th>Nomor Telepon</th>
                            <th>Sumber</th>
                            <th></th>
                            <th></th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($notes as $note)
                            <tr>
                              <td><strong>{{ $note->nama }}</strong></td>
                              <td><strong>{{ $note->status->name }}</strong></td>
                              <td>{{ $note->komoditas }}</td>
                              <td>{{ $note->alamat }}</td>
                              <td>{{ $note->no_hp }}
                              </td>
                              <td>{{ $note->sumber }}</td>
                              <td>
                                <a href="{{ url('/direktori/' . $note->id) }}" class="btn btn-sm btn-primary"><i class="cil-caret-right"></i></a>
                              </td>
                              <td>
                                <a href="{{ url('/direktori/' . $note->id . '/edit') }}" class="btn btn-sm btn-primary"><i class="cil-pencil"></i></a>
                              </td>
                              <td>
                                <form action="{{ route('direktori.destroy', $note->id ) }}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-sm btn-danger"><i class="cil-trash"></i></button>
                                </form>
                              </td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                      {{ $notes->links() }}
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection


@section('javascript')

@endsection

