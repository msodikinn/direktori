@extends('dashboard.base')

@section('content')

               <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-10 col-lg-8 col-xl-6">
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i> Direktori: {{ $note->nama }}</div>
                    <div class="card-body">
                        <br>
                        <h4>Author:</h4>
                        <p> {{ $note->user->name }}</p>
                        <h4>Nama:</h4>
                        <p> {{ $note->nama }}</p>
                        <h4>Komoditas:</h4> 
                        <p>{{ $note->komoditas }}</p>
                        <h4>Alamat:</h4> 
                        <p>{{ $note->alamat }}</p>
                        <h4> Jenis Usaha: </h4>
                        <p>
                              {{ $note->status->name }}
                        </p>
                        <h4>Sumber:</h4>
                        <p>{{ $note->sumber }}</p>
                        <h4>Kota:</h4> 
                        <p>{{ !empty($note->district) ? $note->district->name:'-' }}</p>
                        <h4>Provinsi:</h4> 
                        <p>{{ $note->province->name }}</p>
                        <a href="{{ route('direktori.index') }}" class="btn btn-block btn-primary">{{ __('Return') }}</a>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection


@section('javascript')

@endsection