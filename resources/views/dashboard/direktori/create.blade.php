@extends('dashboard.base')

@section('content')

        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-10 col-lg-8 col-xl-6">
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i> {{ __('Create Direktori') }}</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('direktori.store') }}">
                            @csrf
                            <div class="form-group row">
                                <label>Nama Pelaku Usaha</label>
                                <input class="form-control" type="text" placeholder="{{ __('Nama') }}" name="nama" required autofocus>
                            </div>

                            <div class="form-group row">
                                <label>Jenis Usaha</label>
                                <select class="form-control" name="pelaku_usaha_id" required autofocus>
                                    @foreach($pelaku as $pelaku)
                                        <option value="{{ $pelaku->id }}">{{ $pelaku->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group row">
                                <label>Provinsi</label>
                                <select class="form-control" name="province_id" id="province" autofocus>
                                    <option value="">==Pilih Provinsi==</option>
                                    @foreach($province as $province)
                                    <option value="{{ $province->id }}">{{ $province->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group row">
                                <label>Kota</label>
                                <select class="form-control" name="district_id" id="district" autofocus>
                                    <option value="">==Pilih Kota==</option>
                                </select>
                            </div>

                            <div class="form-group row">
                                <label>Komoditas Utama</label>
                                <input class="form-control" type="text" placeholder="{{ __('Komoditas') }}" name="komoditas"  autofocus>
                            </div>

                            <div class="form-group row">
                                <label>Alamat</label>
                                <textarea class="form-control" id="textarea-input" name="alamat" rows="9" placeholder="{{ __('Alamat..') }}"></textarea>
                            </div>

                            <div class="form-group row">
                                <label>No Telepon</label>
                                <input class="form-control" type="text" placeholder="{{ __('Telepon') }}" name="no_hp"  autofocus>
                            </div>
                            
                            <div class="form-group row">
                                <label>Sumber</label>
                                <textarea class="form-control" id="textarea-input" name="sumber" rows="9" placeholder="{{ __('Sumber..') }}"></textarea>
                            </div>

                            <button class="btn btn-block btn-success" type="submit">{{ __('Add') }}</button>
                            <a href="{{ route('direktori.index') }}" class="btn btn-block btn-primary">{{ __('Return') }}</a> 
                        </form>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection

@section('javascript')
<script>
$(function () {

    $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    });
    
    $('#province').on('change', function () {
        $.ajax({
            url: '{{ route('city-dropdown') }}',
            method: 'POST',
            data: {id: $(this).val()},
            success: function (response) {
                $('#district').empty();

                $.each(response, function (id, name) {
                    $('#district').append(new Option(name, id))
                })
            }
        })
    });
});
</script>
@endsection