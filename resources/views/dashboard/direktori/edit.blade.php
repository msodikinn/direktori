@extends('dashboard.base')

@section('content')

        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-10 col-lg-8 col-xl-6">
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i> {{ __('Edit') }}: {{ $note->nama }}</div>
                    <div class="card-body">
                        <form method="POST" action="/direktori/{{ $note->id }}">
                            @csrf
                            @method('PUT')
                            <div class="form-group row">
                                <div class="col">
                                    <label>Nama Pelaku Usaha</label>
                                    <input class="form-control" type="text" placeholder="{{ __('Nama') }}" name="nama" value="{{ $note->nama }}" required autofocus>
                                </div>
                            </div>

                             <div class="form-group row">
                                <div class="col">
                                    <label>Jenis Pelaku Usaha</label>
                                    <select class="form-control" name="pelaku_usaha_id">
                                        @foreach($pelaku as $pelaku)
                                            @if( $pelaku->id == $note->pelaku_usaha_id )
                                                <option value="{{ $pelaku->id }}" selected="true">{{ $pelaku->name }}</option>
                                            @else
                                                <option value="{{ $pelaku->id }}">{{ $pelaku->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col">
                                    <label>Provinsi</label>
                                    <select class="form-control" name="province_id" id="province">
                                        @foreach($province as $province)
                                            @if( $province->id == $note->province_id )
                                                <option value="{{ $province->id }}" selected="true">{{ $province->name }}</option>
                                            @else
                                                <option value="{{ $province->id }}">{{ $province->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col">
                                    <label>Kota</label>
                                    <select class="form-control" name="district_id" id="district">
                                        @foreach($district as $district)
                                            @if( $district->id == $note->district_id )
                                            <option value="{{ $district->id }}" selected="true">{{ $district->name }}</option>
                                            @endif
                                        @endforeach
                                            <option value="">==Pilih Kota==</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col">
                                    <label>Komoditas Utama</label>
                                    <input class="form-control" type="text" placeholder="{{ __('Komoditas') }}" name="komoditas" value="{{ $note->komoditas }}" autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col">
                                    <label>Alamat</label>
                                    <textarea class="form-control" id="textarea-input" name="alamat" rows="9" placeholder="{{ __('Alamat..') }}">{{ $note->alamat }}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col">
                                    <label>No Telepon</label>
                                    <input class="form-control" type="text" placeholder="{{ __('nohp') }}" name="no_hp" value="{{ $note->no_hp }}" autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col">
                                    <label>Sumber</label>
                                    <textarea class="form-control" id="textarea-input" name="sumber" rows="9" placeholder="{{ __('Sumber..') }}">{{ $note->sumber }}</textarea>
                                </div>
                            </div>
 
                            <button class="btn btn-block btn-success" type="submit">{{ __('Save') }}</button>
                            <a href="{{ route('direktori.index') }}" class="btn btn-block btn-primary">{{ __('Return') }}</a> 
                        </form>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection

@section('javascript')
<script>
$(function () {

    $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    });
    
    $('#province').on('change', function () {
        $.ajax({
            url: '{{ route('city-dropdown') }}',
            method: 'POST',
            data: {id: $(this).val()},
            success: function (response) {
                $('#district').empty();

                $.each(response, function (id, name) {
                    $('#district').append(new Option(name, id))
                })
            }
        })
    });
});
</script>
@endsection