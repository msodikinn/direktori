@extends('dashboard.base')

@section('content')

          <div class="container-fluid">
            <div class="fade-in">
              <div class="row">
                 <div class="col-md-3">
                    <div class="card">
                        <div class="card-header">Direktori Pelaku Usaha</div>
                        <table class="table table-responsive-sm table-striped">
                        <tbody>
                          @foreach($provinces as $p)
                            <tr>
                              <td>
                                <form action="{{url('/')}}" method="GET">
                                  <button class="btn btn-primary-outline" type="submit">
                                    <strong>{{ $p->name }}&nbsp;<span class="badge bg-info">{{ $p->jml }}</span></strong>
                                    {{ Form::hidden('province', $p->id) }}
                                  </button>
                                </form>
                              </td>                           
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="col-md-9">
                  <div class="card">
                      <div class="card-header">
                        <i class="fa fa-align-justify"></i>{{ __('Direktori Pelaku Usaha') }}
                      </div>

                      <div class="card-body">
                          {!! Form::open(['url' => '/', 'method'=>'get', 'class'=>'form-inline'])!!}
                          <div class="form-group {!! $errors->has('nama') ? 'has-error' : '' !!}">
                            {!! Form::text('nama', isset($nama) ? $nama : null, ['class'=>'form-control', 'placeholder' => 'kategori usaha..']) !!}
                            {!! $errors->first('nama', '<p class="help-block">:message</p>') !!}
                            {!! Form::text('komoditas', isset($komoditas) ? $komoditas : null, ['class'=>'form-control', 'placeholder' => 'komoditas utama..']) !!}
                            {!! $errors->first('komoditas', '<p class="help-block">:message</p>') !!}
                          </div>
                            &nbsp;
                            {!! Form::submit('search', ['class'=>'btn btn-info']) !!}
                            
                          {!! Form::close() !!}
                          <br>
                          <table class="table table-responsive-sm table-striped">
                          <thead>
                            <tr>
                              <th>Nama Pelaku Usaha</th>
                              <th>Kategori Usaha</th>
                              <th>Komoditas Utama</th>
                              <th>Alamat</th>
                              <!-- <th>Nomor Telepon</th> -->
                              <!-- <th>Sumber</th> -->
                              <th>Detail</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($direktori as $note)
                              <tr>
                                <td><strong>{{ $note->nama }}</strong></td>
                                <td><strong>{{ $note->status->name }}</strong></td>
                                <td>{{ $note->komoditas }}</td>
                                <td>{{ $note->alamat }}</td>
                                <!-- <td>{{ $note->no_hp }}</td> -->
                                <!-- <td>{{ $note->sumber }}</td> -->
                                <td><a href="{{ url('/home/' . $note->id) }}" class="btn btn-sm btn-info"><i class="cil-caret-right"></td>
                              </tr>
                            @endforeach
                          </tbody>
                        </table>
                        {{ $direktori->appends(compact('nama','province','komoditas'))->links() }}
                      </div>
                  </div>
                </div>
             
              </div>
            </div>
          </div>

@endsection

@section('javascript')

    <script src="{{ asset('js/Chart.min.js') }}"></script>
    <script src="{{ asset('js/coreui-chartjs.bundle.js') }}"></script>
    <script src="{{ asset('js/main.js') }}" defer></script>
@endsection
