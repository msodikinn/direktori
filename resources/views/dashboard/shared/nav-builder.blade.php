<?php
/*
    $data = $menuel['elements']
*/

if(!function_exists('renderDropdown')){
    function renderDropdown($data){
        if(array_key_exists('slug', $data) && $data['slug'] === 'dropdown'){
            echo '<li class="c-sidebar-nav-dropdown">';
            echo '<a class="c-sidebar-nav-dropdown-toggle" href="#">';
            if($data['hasIcon'] === true && $data['iconType'] === 'coreui'){
                echo '<i class="' . $data['icon'] . ' c-sidebar-nav-icon"></i>';    
            }
            echo $data['name'] . '</a>';
            echo '<ul class="c-sidebar-nav-dropdown-items">';
            renderDropdown( $data['elements'] );
            echo '</ul></li>';
        }else{
            for($i = 0; $i < count($data); $i++){
                if( $data[$i]['slug'] === 'link' ){
                    echo '<li class="c-sidebar-nav-item">';
                    echo '<a class="c-sidebar-nav-link" href="' . url($data[$i]['href']) . '">';
                    echo '<span class="c-sidebar-nav-icon"></span>' . $data[$i]['name'] . '</a></li>';
                }elseif( $data[$i]['slug'] === 'dropdown' ){
                    renderDropdown( $data[$i] );
                }
            }
        }
    }
}
?>

      <div class="c-sidebar-brand"><img class="c-sidebar-brand-full" src="{{ url('/assets/brand/logo.png') }}" width="118" height="46" alt="CoreUI Logo"><img class="c-sidebar-brand-minimized" src="{{ url('/assets/brand/logo.png') }}" width="118" height="46" alt="CoreUI Logo mobile"></div>
        <ul class="c-sidebar-nav">
            <li class="c-sidebar-nav-item">
              <a class="c-sidebar-nav-link" href="/">
                <i class="c-sidebar-nav-icon cil-speedometer"></i> Home</a>
            </li>
            @if (Route::has('login'))
            @auth
            <li class="c-sidebar-nav-item">
              <a class="c-sidebar-nav-link" href="/pelaku">
                <i class="c-sidebar-nav-icon cil-drop"></i> Kategori Usaha</a>
            </li>
            <li class="c-sidebar-nav-item">
              <a class="c-sidebar-nav-link" href="/direktori">
                <i class="c-sidebar-nav-icon cil-layers"></i> Direktori Usaha</a>
            </li>
            @if (Auth::user()->can('user-list'))
            <li class="c-sidebar-nav-item">
              <a class="c-sidebar-nav-link" href="/users">
                <i class="c-sidebar-nav-icon cil-contact"></i> Management User</a>
            </li>
            @endif
            @else
            <li class="c-sidebar-nav-item">
              <a class="c-sidebar-nav-link" href="/login">
                <i class="c-sidebar-nav-icon cil-account-logout"></i> Login</a>
            </li>
            @endauth
            @endif
        </ul>
        <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent" data-class="c-sidebar-minimized"></button>
    </div>