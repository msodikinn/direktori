<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PelakuUsaha extends Model
{
    protected $table = 'pelaku_usaha';
    public $timestamps = false; 
}
