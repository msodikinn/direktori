<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class District extends Model
{
    use HasFactory;

    protected $table = 'district';
    public $timestamps = false; 
    /**
     * Get the notes for the status.
     */
    public function direktori()
    {
        return $this->hasMany('App\Models\Direktori');
    }
}
