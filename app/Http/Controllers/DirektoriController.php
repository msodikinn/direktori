<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Direktori;
use App\Models\PelakuUsaha;
use App\Models\Province;
use App\Models\District;

class DirektoriController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notes = Direktori::with('user')->with('status')->paginate( 20 );
        return view('dashboard.direktori.direktoriList', ['notes' => $notes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pelaku = PelakuUsaha::all();
        $province = Province::all();
        return view('dashboard.direktori.create', [ 'pelaku' => $pelaku, 'province' => $province ]);
    }

    public function getdistrict(Request $request)
    {
        $cities = District::where('province_id', $request->get('id'))
            ->pluck('name', 'id');
    
        return response()->json($cities);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama'             => 'required|min:1|max:64',
            'pelaku_usaha_id'   => 'required'
        ]);
        $user = auth()->user();
        $note = new Direktori();
        $note->nama     = $request->input('nama');
        $note->no_hp   = $request->input('no_hp');
        $note->pelaku_usaha_id = $request->input('pelaku_usaha_id');
        $note->province_id = $request->input('province_id');
        $note->district_id = $request->input('district_id');
        $note->alamat = $request->input('alamat');
        $note->sumber = $request->input('sumber');
        $note->komoditas = $request->input('komoditas');
        $note->users_id = $user->id;
        $note->save();
        $request->session()->flash('message', 'Successfully created Direktori');
        return redirect()->route('direktori.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $note = Direktori::with('user')->with('status')->with('province')->with('district')->find($id);
        return view('dashboard.direktori.direktoriShow', [ 'note' => $note ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $note = Direktori::find($id);
        $pelaku = PelakuUsaha::all();
        $province = Province::all();
        $district = District::all();
        return view('dashboard.direktori.edit', [ 'pelaku' => $pelaku, 'note' => $note, 'province' => $province, 'district' => $district ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //die();
        $validatedData = $request->validate([
            'nama'             => 'required|min:1|max:64',
            'pelaku_usaha_id'   => 'required'
        ]);
        $note = Direktori::find($id);
        $note->nama     = $request->input('nama');
        $note->no_hp   = $request->input('no_hp');
        $note->pelaku_usaha_id = $request->input('pelaku_usaha_id');
        $note->alamat = $request->input('alamat');
        $note->sumber = $request->input('sumber');
        $note->komoditas = $request->input('komoditas');
        $note->province_id = $request->input('province_id');
        $note->district_id = $request->input('district_id');
        $note->save();
        $request->session()->flash('message', 'Successfully edited direktori');
        return redirect()->route('direktori.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $note = Direktori::find($id);
        if($note){
            $note->delete();
        }
        return redirect()->route('direktori.index');
    }
}
