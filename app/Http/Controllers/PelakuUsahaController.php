<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\PelakuUsaha;
use App\Models\Status;

class PelakuUsahaController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notes = PelakuUsaha::paginate( 20 );
        //dd($notes);
        return view('dashboard.pelaku.pelakuList', ['notes' => $notes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $statuses = Status::all();
        //dd($statuses);
        return view('dashboard.pelaku.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'name'             => 'required|min:1|max:64'
        ]);
        //$user = auth()->user();
        $note = new PelakuUsaha();
        $note->name     = $request->input('name');
        // $note->content   = $request->input('content');
        // $note->status_id = $request->input('status_id');
        // $note->note_type = $request->input('note_type');
        // $note->applies_to_date = $request->input('applies_to_date');
        // $note->users_id = $user->id;
        $note->save();
        $request->session()->flash('message', 'Successfully created Pelaku Usaha');
        return redirect()->route('pelaku.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $note = PelakuUsaha::find($id);
        return view('dashboard.pelaku.pelakuShow', [ 'note' => $note ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $note = PelakuUsaha::find($id);
        $statuses = Status::all();
        return view('dashboard.pelaku.edit', [ 'statuses' => $statuses, 'note' => $note ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //var_dump('bazinga');
        //die();
        $validatedData = $request->validate([
            'name'             => 'required|min:1|max:64'
        ]);
        $note = PelakuUsaha::find($id);
        $note->name     = $request->input('name');
        // $note->content   = $request->input('content');
        // $note->status_id = $request->input('status_id');
        // $note->note_type = $request->input('note_type');
        // $note->applies_to_date = $request->input('applies_to_date');
        $note->save();
        $request->session()->flash('message', 'Successfully edited Pelaku Usaha');
        return redirect()->route('pelaku.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $note = PelakuUsaha::find($id);
        if($note){
            $note->delete();
        }
        return redirect()->route('pelaku.index');
    }
}
