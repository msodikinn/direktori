<?php

namespace App\Http\Controllers;
use DB;
use App\Models\Direktori;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $provinces = DB::select('select p.id, p.name, count(1) as jml from direktori d LEFT JOIN province p on p.id = d.province_id group by p.id, p.name');

        $province = $request->get('province');
        $nama = null;
        $komoditas = null; 
        $direktori = Direktori::query();
        if ($province != null) {
            $direktori = $direktori->where('province_id',$province);

            if ($request->has('nama')) {
                $nama = $request->get('nama');
                $direktori = $direktori->where('nama', 'LIKE', '%'.$nama.'%')->where('province_id',$province);
            }
            if ($request->has('komoditas')) {
                $komoditas = $request->get('komoditas');
                $direktori = $direktori->where('komoditas', 'LIKE', '%'.$komoditas.'%')->where('province_id',$province);
            }
        }
        if ($request->has('nama')) {
            $nama = $request->get('nama');
            $direktori = $direktori->where('nama', 'LIKE', '%'.$nama.'%');
        }
        if ($request->has('komoditas')) {
            $komoditas = $request->get('komoditas');
            $direktori = $direktori->where('komoditas', 'LIKE', '%'.$komoditas.'%');
        }
        $direktori = $direktori->paginate( 10 );

        //$direktori = Direktori::where('nama', 'LIKE', '%'.$nama.'%')->where('province_id',$province)
        //             ->with('province')->paginate( 10 );

        return view('dashboard.homepage', compact('province', 'provinces', 'direktori', 'nama', 'komoditas'));
        
    }

    public function show($id)
    {
        $note = Direktori::with('user')->with('status')->with('province')->find($id);
        return view('dashboard.homepageShow', [ 'note' => $note ]);
    }
}
